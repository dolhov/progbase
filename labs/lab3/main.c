#include <stdio.h>
#include <math.h>
#include <stdbool.h>

double fy(double x) // function from lab2
{
    double y = 0;
    if (x == 0)
    {
        return false;
    }
    else if (-5 < x && x <= 3)
    {
        y = pow((x + 3), 3) + 1 / (x);
    }
    else 
    {
        y = 0.3 * tan(x) + 2;
    }
    return y;
}


double int_fy(double x_min,double x_max,double x_step) // function of integer calculation
{
    double y = 0;
    double S = 0;
    double Area = 0;
    while (x_min < x_max)
    {
        y = fy(x_min + x_step);
        S = y * x_step;
        Area += S;
        x_min += x_step;
    }
    return Area;
}


int main()
{
    double x_min = 0;
    double x_max = 0;
    double x_step = 0;
    double pi = 3.141592653589793; // pi number
    double test = pi/2;
    int st_test = 1;
    double area = 0;
    printf("Enter min, max, step:");
    scanf("%lf %lf %lf", &x_min, &x_max, &x_step);
    if (x_min == x_max)                             // various tests for input
        puts("ERROR: min can't be equal to max\n");
    else if (x_step == 0)
        puts("ERROR: step can't be equal to zero\n");
    else if (x_min > x_max)
        puts("ERROR: min can't be more than max\n");
    else if (x_min < 0 && x_max < 0 ) 
    {
        test = -5 * pi / 2; // first period < 0
        if (x_max < -100) // reduces number of iteration for the next test loop
        {
            st_test = x_min / 10;
            test += st_test * 3 * pi;
        }
        while (test > x_max) // check if segment is in range if min,max <= -5
        {
            test -= pi;
        }
        if (test > x_min)
            puts("ERROR: invalid range\n");
        else
        {
            area = int_fy(x_min, x_max, x_step);
            printf("Integer approximately equals to %lf\n", area);
        }
    }
    else if (x_min > 0 && x_max > 0)
    {
        test = 3 * pi / 2;
        if (x_min > 100) // reduces number of iteration for the next test loop
        {
            st_test = x_min / 10;
            test += st_test * 3 * pi;
        }
        while (test < x_min) // check if segment is in range if min,max > 3
        {
            test += pi;
        }
        if (test < x_max)
            puts("ERROR: invalid range\n");
        else
        {
            area = int_fy(x_min, x_max, x_step);
            printf("Integer approximately equals to %lf\n", area);
        }
    }
    else
    {
        puts("ERROR: invalid range\n");
    }
}
