#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int conReadLine(char str[], int maxBufLen)
{
    fgets(str, maxBufLen, stdin);
    int bufLength = strlen(str);
    if (str[bufLength - 1] == '\n')
    {
        str[bufLength - 1] = '\0';
        bufLength -= 1;
    }
    else
    {
        for (char ch; (ch = getchar()) != '\n';)
        {
        }
    }
    return bufLength;
}


void CharLower(char command[])
{
    if (strcmp(command, "char:lower") == 0)
    {
        for (unsigned char i = 0; i < 128; i++)
        {
            if (islower(i))
            {
                printf("%c ", i);
            }
        }
        printf("\n");
    }
    else
    {
        printf("Unknown class \"%s\"", command + 5);
    }
}

void CharDigit(char command[])
{
    if (strcmp(command, "char:digit") == 0)
    {
        for (unsigned char i = 0; i < 128; i++)
        {
            if (isdigit(i))
            {
                printf("%c ", i);
            }
        }
        printf("\n");
    }
    else
    {
        printf("Unknown class \"%s\"", command + 5);
    }
}

void CharAlnum(char command[])
{
    if (strcmp(command, "char:alnum") == 0)
    {
        for (unsigned char i = 0; i < 128; i++)
        {
            if (isalnum(i))
            {
                printf("%c ", i);
            }
        }
        printf("\n");
    }
    else
    {
        printf("Unknown class \"%s\"", command + 5);
    }
}


void CharPunct(char command[])
{
    if (strcmp(command, "char:punct") == 0)
    {
        for (unsigned char i = 0; i < 128; i++)
        {
            if (ispunct(i))
            {
                printf("%c ", i);
            }
        }
        printf("\n");
    }
    else
    {
        printf("Unknown class \"%s\"", command + 5);
    }
}


void CharGraph(char command[])
{
    if (strcmp(command, "char:graph") == 0)
    {
        for (unsigned char i = 0; i < 128; i++)
        {
            if (isgraph(i))
            {
                printf("%c ", i);
            }
        }
        printf("\n");
    }
    else
    {
        printf("Unknown class \"%s\"", command + 5);
    }
}




//functions for 2nd task
void length(char command[], char arrstr[]) 
{
    if (strcmp(command, "string:length") == 0)
    {
        int len = strlen(arrstr);
        printf("The length of the string is %i\n", len);
    }
    else
    {
        printf("Unknown class \"%s\"\n", command + 7);
    }
}

void clear(char command[], char arrstr[])
{
    if (strcmp(command, "string:clear") == 0)
    {
        for (int i = 0; i < 32; i++)
        {
            arrstr[i] = '\0';
        }
        printf("The string now is \"%s\"\n", arrstr);
    }
    else
    {
        printf("Unknown class \"%s\"\n", command + 7);
    }
}

void concat(char command[], char arrstr[])
{
    if (strncmp(command, "string:concat:", 14) == 0)
    {
        strncat(arrstr, command + 14, 32 - strlen(arrstr));
        printf("The new string is \"%s\"\n", arrstr);
    }
    else
    {
        printf("Unknown class \"%s\"\n", command + 7);
    }
}

void StringLower(char command[], char arrstr[])
{
    if (strcmp(command, "string:lower") == 0)
    {
        for (int i = 0; i < 32; i++)
        {
            if (isprint(arrstr[i]))
            {
                arrstr[i] = tolower(arrstr[i]);
            }
        }
        printf("The string now is \"%s\"\n", arrstr);
    }
    else
    {
        printf("Unknown class \"%s\"\n", command + 7);
    }
}

void substr(char command[], char arrstr[])
{
    if (isdigit(command[14]))
    {
        char *z;
        char buff[32] = "\0";
        int from = strtol(command + 14, &z, 10);
        int len = -1;
        if (from < 0 || from > 30)
        {
            printf("ERROR: incorrect input to class {length}\n");
        }
        else 
        {
            len = strtol(z + 1, &z, 10);
        }
        if (from < 32 - len && &z == NULL)
        {
            strncat(buff, arrstr + from, len);
            printf("From %i, %i symbols: \"%s\"\n", from, len, buff);
            buff[0] = '\0';
            from = 0;
            len = 0;
        }
        else
        {
            printf("ERROR: incorrect input to class {length}\n");
        }
    }
    else
    {
        printf("Incorrect input to {from} or to {length}\n");
    }
}


void contains(char command[], char arrstr[])
{
    if (strlen(command) == 17)
    {
        if (strchr(arrstr, command[16]) != NULL)
        {
            printf("Yes, the string contains symbol \"%s\"\n", command + 16);
        }
        else
        {
            printf("No, the string doesn't contain symbol \"%s\"\n", command + 16);
        }
    }
    else
    {
        printf("ERROR:Too many symbols, choose one\n");
    }
}

//functions for 3rd task
void StringsGet(char command[], char  strings[][32])
{
    if (strncmp(command, "strings:get:", 12) == 0)
    {
        if (isdigit(command[12]) )
        {
            int index = atoi(command + 12);
            if (index <= 9)
                printf("[%i] %s\n", index, strings[index]);
            else
            {
                printf("ERROR: invalid input to {index}\n");
            }
            
        }
        else
        {
            printf("ERROR: invalid input to {index}\n");
        }
    }
    else
    {
        printf("ERROR: unknown class \"%s\"", command + 7);
    }
}


void StringsSet(char command[], char strings[][32])
{
    if (strncmp(command, "strings:set:", 12) == 0)
    {
        if (isdigit(command[12]))
        {
            int index = atoi(command + 12);
            strings[index][0] = '\0';
            strcat(strings[index], command + 14);
            printf("New string: \n[%i] %s\n", index, strings[index]);
        }
        else
        {
            printf("ERROR: invalid input to {index}\n");
        }
    }
    else
    {
        printf("Error: unknown class \"%s\"", command + 7);
    }
}


void StringsCount(char command[], char strings[][32])
{
    if (strcmp(command, "strings:count:exclamation") == 0)
    {
        int counter = 0;
        for (int i = 0; i < 10; i++)
        {
            if (strchr(strings[i], '!') != NULL)
            {
                counter += 1;
            }
        }
        printf("The number of strings that contain \"!\" is %i\n", counter);
    }
    else
    {
        printf("Error: unknown class \"%s\"\n", command + 7);
    }
}

void StringsContain(char command[],char strings[][32])
{
    bool check = true;
    for (int i = 0; i < 10; i++)
    {
        if (strcmp(strings[i], command + 16) == 0)
        {
            printf("Yes, array contains string you entered\n");
            check = false;
        }
    }
    if (check == true)
    {
        printf("No, array doesn't contain string you entered\n");
    }
}


void StringsStrings(char strings[][32])
{
    for (int i = 0; i < 10; i++)
    {
        printf("[%i] %s\n", i, strings[i]);
    }
}

int main()
{
    char strings[10][32] = {
                            "Half a league, half a league,",
                            "Half a league onward,",
                            "All in the valley of Death",
                            "Rode the six hundred.",
                            "\"Forward, the Light Brigade!",
                            "Charge for the guns!\" he said.",
                            "Into the valley of Death",
                            "Rode the six hundred.",
                            "",
                            ""
                            };
    
    char arrstr[32] = "Your smile is beautiful :)";
    char buff[32] = "\0";
    int from = 0;
    int len = 0;
    while (1 > 0)
    {
        printf("Enter command: ");
        const int bufLen = 100;
        char command[bufLen];
        int strLen = conReadLine(command, bufLen);
        printf("Entered (%d): \"%s\"", strLen, command);
        puts("\n---");
        if (strncmp(command, "char:", 5) == 0)              // 1 task start
        {
            if (strncmp(command + 5, "lower", 5) == 0)
            {
                CharLower(command);
            }
            else if (strncmp(command + 5, "digit", 5) == 0)
            {
                CharDigit(command);
            }
            else if (strncmp(command + 5, "alnum", 5) == 0)
            {
                CharAlnum(command);
            }
            else if (strncmp(command + 5, "punct", 5) == 0)
            {
                CharPunct(command);
            }
            else if (strncmp(command + 5, "graph", 5) == 0)
            {
                CharGraph(command);
            }
            else
            {
                printf("Error: unknown class \"%s\"", command + 5);    // 1 task end
            }
            printf("\n");
        }
        else if ((strncmp(command, "strings", 7) == 0)) // 3 task start
        {
            if ((strncmp(command + 8, "get:", 4) == 0))
            {
                StringsGet(command, strings);
            }
            else if (strncmp(command + 8, "set:", 4) == 0)
            {
                StringsSet(command, strings);
            }
            else if (strncmp(command + 7, ":count", 6) == 0)
            {
                StringsCount(command, strings);
            }
            else if (strncmp(command + 7, ":contain:", 9) == 0)
            {
                StringsContain(command, strings);
            }
            else if (strcmp(command, "strings") == 0)
            {
                StringsStrings(strings);
            }
            else
            {
                printf("Unknown command \"%s\"\n", command);
            }
        }
        else if ((strncmp(command, "string", 6) == 0))                  // 2 task
        {
            if ((strncmp(command, "string:", 7) == 0))
            {
                if (strncmp(command + 7, "length", 6) == 0)
                {
                    length(command, arrstr);
                }
                else if (strncmp(command + 7, "clear", 5) == 0)
                {
                    clear(command, arrstr);
                }
                else if (strncmp(command + 7, "concat", 6) == 0)
                {
                    concat(command, arrstr);
                }
                else if (strncmp(command + 7, "lower", 5) == 0)
                {
                    StringLower(command, arrstr);
                }
                else if (strncmp(command + 7, "substr", 6) == 0)
                {
                    substr(command, arrstr);
                }
                else if (strncmp(command + 7, "contains:", 9) == 0)
                {
                    contains(command, arrstr);
                }
                else
                {
                    printf("Unknown class \"%s\"\n", command + 7);
                }
            }
            else if (strcmp(command, "string") == 0)
            {
                printf("The string is: \"%s\"\n", arrstr);
            } 
            else
            {
                printf("Unknown command \"%s\"\n", command);
            }
        }
        else if (strcmp(command, "exit") == 0)
        {
            puts("Finish the program");
            break;
        }
        else if (1 > 0)
        {
            printf("Unknown command \"%s\"\n", command);
        }
    }
}
