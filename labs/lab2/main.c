#include <stdio.h>
#include <math.h>

int main()
{   
    float exp = 0;
    float x = -10;
    float step = 0.5;
    while (x <= 10)
    {
    if (x == 0)
    {
        printf("y(%.1f) = ERROR\n", x);
    }
    else if (-5 < x && x <= 3)
        {
            exp = pow((x + 3), 3) + 1 / (x);
        }

    else 
        {
            exp = 0.3 * tan(x) + 2;
        }
        if (x != 0)
        {
        printf("y(%.1f) = ", x);
        printf("%f\n", exp);
        }
        x += step;
    }
}
