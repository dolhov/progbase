#include <stdio.h>
#include <stdlib.h>
#include <math.h>


struct DynArray{
    int * array;
    size_t capacity;
    size_t length;
};


struct SLNode 
{
    float data;
    struct SLNode * next;
};






struct DynArray dynarray_init();
void dynarray_deinit(struct DynArray * darr);
void dynarray_clear(struct DynArray * darr);
void dynarray_realloc(struct DynArray * pdarr);
void dynarray_resize(struct DynArray * pdarr, size_t new_length, int value);
void dynarray_print(struct DynArray * pdarr);
void dynarray_insert(struct DynArray * pdarr ,int index, int value);
void dynarray_pop_front(struct DynArray * pdarr);
void dynarray_main();


struct SLNode * slnode_create(float data);
void slnode_print(struct SLNode * node);
struct SLNode * slnode_push_front(struct SLNode * head, float new_value);
struct SLNode * slnode_back(struct SLNode * node);
struct SLNode * slnode_push_back(struct SLNode * head, float new_value);
void slnode_clear(struct SLNode * head);
int slnode_size(struct SLNode * head);
int slnode_count_pos(struct SLNode * head);
float slnode_sum(struct SLNode * head);
void slnode_main();

int main()
{
    dynarray_main();
    slnode_main();
}





struct DynArray dynarray_init() 
{
    const size_t initial_capacity = 16;
    return (struct DynArray) {
        malloc(initial_capacity * sizeof(int)),
        initial_capacity,
        0
    };
}


void dynarray_deinit(struct DynArray * darr)
{
    free(darr->array);
    darr->array = NULL;
    darr->capacity = 0;
    darr->length = 0;
}


void dynarray_clear(struct DynArray * darr)
{
    darr->length = 0;
}


void dynarray_realloc(struct DynArray *pdarr) {
    pdarr->capacity *= 2;
    int *new_array = realloc(pdarr->array, pdarr->capacity * sizeof(int));
    if (new_array == NULL) {
        free(pdarr->array);
        fprintf(stderr, "Memory reallocation error for %s", __func__);
        exit(1);
    }
    else pdarr->array = new_array;
}


void dynarray_resize(struct DynArray * pdarr, size_t new_length, int value)
{
    int * new_array;
    if (new_length > pdarr->capacity)
    {
        new_array = realloc(pdarr->array, new_length*sizeof(int));
    }
    if (new_array == NULL)
    {
        free(pdarr->array);
        fprintf(stderr, "Memory allocation error for %s", __func__);
        exit(1);
    }
    else
    {
        pdarr->array = new_array;
        pdarr->capacity = new_length;
    }
    for (int i = 0; i < new_length; i++)
    {
        pdarr->array[i] = value;
    }
    pdarr->length = new_length;
}


void dynarray_print(struct DynArray * pdarr)
{
    if (pdarr->array == NULL)
    {
        printf("NULL\n\n\n");
    }
    else
    {
        printf("Capacity of dyn_arr: %zu \n", pdarr->capacity);
        printf("Length of dyn_arr: %zu \n", pdarr->length);
        printf("Elements of dyn_arr: ");
        for (int i = 0; i < pdarr->length; i++)
        {
            printf("%i ", pdarr->array[i]);
        }
        printf("\n\n");
    }
}


void dynarray_insert(struct DynArray * pdarr ,int index, int value)
{
    if (index < pdarr->length + 1 && index >= 0)
    {
        if (pdarr->length == pdarr->capacity)
        {
            dynarray_realloc(pdarr);
        }
        for (int i = pdarr->length; i > index; i--)
        {
            pdarr->array[i] = pdarr->array[i - 1];
        }
        pdarr->array[index] = value;
        pdarr->length += 1;
    }
}



void dynarray_pop_front(struct DynArray * pdarr)
{
    for (int i = 0; i < pdarr->length; i++)
    {
        pdarr->array[i] = pdarr->array[i+1];
    }
    pdarr->length -= 1;
}


void dynarray_main()
{
    struct DynArray a = dynarray_init();
    dynarray_print(&a);
    dynarray_resize(&a, 20, 13);
    dynarray_print(&a);
    dynarray_insert(&a, 1, 5);
    dynarray_print(&a);
    dynarray_pop_front(&a);
    dynarray_print(&a);
    dynarray_clear(&a);
    dynarray_print(&a);
    dynarray_deinit(&a);
    dynarray_print(&a);
}


struct SLNode * slnode_create(float data)
{
    struct SLNode * pnode = malloc(sizeof(struct SLNode));
    pnode->data = data;
    pnode->next = NULL;
    return pnode;
}


void slnode_print(struct SLNode * node)
{
    while (node != NULL)
    {
        printf("%f -> ", node->data);
        node = node->next;
    }
    printf("NULL\n");
}

struct SLNode * slnode_push_front(struct SLNode * head, float new_value) 
{
    struct SLNode * new_node = slnode_create(new_value);
    new_node->next = head; 
    return new_node;
}


struct SLNode * slnode_back(struct SLNode * node)
{
    while (node->next != NULL)
    {
        node = node->next;
    }
    return node;
}

struct SLNode * slnode_push_back(struct SLNode * head, float new_value) 
{
    struct SLNode * new_node = slnode_create(new_value); 
    if (head == NULL) {
        return new_node;
    } else {
        struct SLNode * back = slnode_back(head);
        back->next = new_node;
        return head;
    }
}



void slnode_clear(struct SLNode * head)
{
    struct SLNode * node = head;
    while (node != NULL)
    {
        struct SLNode * next = node->next;
        free(node);
        node = next;
    }
}

int slnode_size(struct SLNode * head)
{
    int counter = 0;
    while (head != NULL)
    {
        counter += 1;
        head = head->next;
    }
    return counter;
}


int count_pos(struct SLNode * head)
{
    int counter = 0;
    while (head != NULL)
    {
        if (head->data > 0)
        {
            counter += 1;
        }
    }
    return counter;
}


float slnode_sum(struct SLNode * head)
{
    float sum = 0;
    if (head == NULL)
    {
        return NAN;
    }
    while (head != NULL)
    {
        sum += head->data;
        head = head->next;
    }
    return sum;
}


int slnode_count_pos(struct SLNode * head)
{
    int counter = 0;
    while (head != NULL)
    {
        if (head->data > 0)
        {
            counter += 1;
        }
        head = head->next;
    }
    return counter;
}


void slnode_main()
{
    struct SLNode * head = NULL;
    head = slnode_push_back(head, -3.14);
    slnode_print(head);
    slnode_push_back(head, 3.5);
    slnode_print(head);
    head = slnode_push_front(head, -2);
    slnode_print(head);
    printf("Size is: %i\n", slnode_size(head));
    printf("Sum is: %f\n", slnode_sum(head));
    printf("Number of positive elements: %i\n", slnode_count_pos(head));
    printf("\nUse of slnode_clear():\n\n");
    slnode_clear(head);
    head = NULL;
    printf("Sum is: %f\n", slnode_sum(head));
    slnode_print(head);
}