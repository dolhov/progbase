#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

double fy(double x) // function from lab2
{
    double y = 0;
    if (-5 < x && x <= 3)
    {
        y = pow((x + 3), 3) + 1 / (x);
    }
    else 
    {
        y = 0.3 * tan(x) + 2;
    }
    return y;
}

void int_matrix(void) // task 1
{
    srand(time(0));
    int A[30] = {0};
    int B[20] = {0};
    int C[20] = {0};
    for (int i = 0; i < 30; i++)
    {
        A[i] = rand() % (20) + 0;
    }
    for (int i = 0; i < 30; i++)
    {
        if (A[i] >= 0 && A[i] <= 19)
        {
            B[A[i]] += 1;
        }
    }
    puts("Array A: ");
    for (int i = 0; i < 30; i++)
        printf("%i)%i; ", i, A[i]);
    printf("\n");
    puts("Array B: ");
    for (int i = 0; i < 20; i++)
        printf("%i)%i; ", i, B[i]);
}

void float_matrix(void) // task 2
{
    float M[2][20];
    float x_min = 0;
    float x_max = 0;
    printf("Enter min: ");
    scanf("%f", &x_min);
    printf("Enter max: ");
    scanf("%f", &x_max);
    if (x_min >= x_max)
        puts("Min can't be equal or more than max");
    else 
    {
        float segm = (x_max - x_min) / 20;
        for (int i = 0; i < 20; i++)
        {
            float a = x_min + i * segm;
            M[0][i] = a;
        } 
        for (int i = 0; i < 20; i++)
        {
            if (M[0][1] == 0)
                continue;
            M[1][i] = fy(M[0][i]);
        }
    
        for (int i = 0; i < 20; i++)
        {
            printf("%i)%f; ", i, M[0][i]);
        }
        puts("\n");
        for (int i = 0; i < 20; i++)
        {
            if (M[0][i] == 0)
                printf("%i)not defiend;", i);
            else
                printf("%i)%f; ", i, M[1][i]);
        }
    }
}

int main() // all together
{
    while (1 > 0)
    {
        float choose = 0;
        bool mybreak = false;
        if (choose == 0);
        {
            printf("Enter option number\n");
            puts("1. Int array");
            puts("2. Float matrix");
            puts("3. Quit");
            scanf("%f", &choose);
            printf("\n");
        }
        if (choose == 1)
        {
            while (1 > 0)
            {
                int_matrix();
                printf("\n\n");
                while (1 > 0)
                {
                    choose = 0;
                    puts("Enter option number");
                    puts("1. Do again");
                    puts("2. Quit to menu");
                    scanf("%f", &choose);
                    printf("\n");
                    if (choose == 1)
                        break;
                    else if (choose == 2)
                    {
                        mybreak = true;
                        break;
                    }
                    else
                    {
                        printf("ERROR: invalid value, enter option number again");
                    }
                }
                if (mybreak == true)
                {
                    mybreak = false;
                    break;
                }
            }
            continue;
        }
        else if (choose == 2)
        {
            while (1 > 0)
            {
                float_matrix();
                printf("\n\n");
                while (1 > 0)
                {
                    choose = 0;
                    puts("Enter option number");
                    puts("1. Do again");
                    puts("2. Quit to menu");
                    scanf("%f", &choose);
                    printf("\n");
                    if (choose == 1)
                        break;
                    else if (choose == 2)
                    {
                        mybreak = true;
                        break;
                    }
                    else
                    {
                        printf("ERROR: invalid value, enter option number again\n");
                    }
                }
                if (mybreak == true)
                {
                    mybreak = false;
                    break;
                }
            }
            continue;
        }
        else if (choose == 3)
        {
            while (1 > 0)
            {
            printf("Are you sure want to finish program?\n");
            puts("1. Yes");
            puts("2. No");
            scanf("%f", &choose);
            if (choose == 1)
            {
                printf("\n");
                mybreak = true;
                break;
            }
            else if (choose == 2)
            {
                printf("\n");
                break;
            }
            else
                puts("ERROR: invalid input value");
            }
            if (mybreak == true)
            {
                mybreak = false;
                break;
            }
            continue;
        }
        else
        {
            puts("ERROR: invalid input value\n");
        }
    }
}