#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <progbase.h>
#include <progbase/console.h>
#include <progbase/canvas.h>
#include <time.h>

const float pi = 3.141592;


struct Vec2Cart
{
   float x;
   float y;
};

struct Vec2Polar
{
    float radius;
    float angle;
};

struct Circle
{
    struct Vec2Cart origin;
    float radius2;
};



struct Vec2Cart p2c(struct Vec2Polar vpolar);
struct Vec2Cart vcadd(struct Vec2Cart v1, struct Vec2Cart v2);




int main()
{
    srand(time(0));
    int w = 50;
    int h = 50;
    int length = 20;
    Canvas_setSize(w, h);
    Canvas_invertYOrientation();
    int FPS = 30;
    float delay = 1000 / FPS;
    const int N = 7;
    struct Vec2Cart points[N * 2];

    struct Circle circles[N];
    const float dt = delay / 1000.0;
    const float dAlpha = 2 * pi / (N);
    float alpha = 0;
    struct Vec2Cart C = {w/2, h/2};
    struct Vec2Polar A = {length, 0};
    struct Vec2Polar A1 = {length, 0};
    struct Vec2Polar move = {length, 0};
    int counter = 0;
    float t = 1;
    float t1 = 1;
    float q = 0.1;
    int g = 0;
    float rmax = dAlpha;
    float rmin = 0;

    while (!Console_isKeyDown())
    {
        //
        int b = t;
        q = t;
        int j = 0;
        int counter = 0;
        for (int i = 0; i < N * 2; i += 1)
        {
            if (j % 2 == 0)
            {
                struct Vec2Polar point = A;
                if (i % 2 == 0)
                {
                    point.angle += dAlpha * i;
                }
                else
                {
                    point.angle += dAlpha * (i-1) + pi;
                    j += 1;
                }


                points[i] = vcadd(C, p2c(point));
                if (i % 2 == 1)
                {
                    struct Vec2Polar origin = {(point.radius * sin(point.angle/ (t1/2) )), point.angle};
                    circles[counter].origin = vcadd(C,p2c(origin));
                    circles[counter].radius2 = 2;
                    counter += 1;
                }
            }
            else 
            {
                struct Vec2Polar point = A1;
                if (i % 2 == 0)
                {
                    point.angle += dAlpha * (i);
                }
                else
                {
                    point.angle += dAlpha * (i - 1) + pi ;
                    j += 1;
                }
                points[i] = vcadd(C, p2c(point));
                if (i % 2 == 1)
                {
                    struct Vec2Polar origin = {(point.radius * sin(point.angle/(t1/2))),point.angle};
                    circles[counter].origin = vcadd(C, p2c(origin));
                    circles[counter].radius2 =  3;
                    counter += 1;
                }
            }
        }

        Canvas_beginDraw();
        Canvas_setColorRGB(0, 58, 0);

        Canvas_putPixel(0, 0);
        Canvas_putPixel(C.x, C.y);
        for (int i = 0 ; i < N * 2; i += 2)
        {
            Canvas_setColorRGB(abs(255 * cos(t*100/(5 * i)/delay)),  abs(200 * (sin(t/delay))) + 55, 150);
            Canvas_strokeLine(points[i].x, points[i].y, points[i+1].x, points[i+1].y);
        }
        Canvas_setColorRGB(0, 0, 200);
        for (int i = 0; i < N; i++)
        {
            Canvas_setColorRGB(abs(255 * (sin(t*100/i)/delay)),  abs(255 * cos(t*100/(5 * i)/delay)) , abs(200 * (sin(t*100/i)/delay))+ 55);
            if (i%2 == 1)
                circles[i].radius2 = 3;
            else 
                circles[i].radius2 = 2;
            Canvas_fillCircle(circles[i].origin.x, circles[i].origin.y, circles[i].radius2);
        }
        
        Canvas_endDraw();
        if (t1 > 10)
            t1 = 1;
        A.angle += pi * dt + sin(0.000000001/(t1));
        A1.angle -= pi * dt/2 + sin(0.0000000021/(t1));
        t += dt;
        t1 += dt;
        sleepMillis(delay);
    }
}



struct Vec2Cart p2c(struct Vec2Polar vpolar) {
    struct Vec2Cart vcart = {
        vpolar.radius * cos(vpolar.angle),
        vpolar.radius * sin(vpolar.angle),
    };
    return vcart;
};

struct Vec2Cart vcadd(struct Vec2Cart v1, struct Vec2Cart v2) {
    struct Vec2Cart vres = {
        v1.x + v2.x,
        v1.y + v2.y,
    };
    return vres;
}