
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <progbase.h>
#include <progbase/console.h>
#include <progbase/canvas.h>


struct Point
{
   int x;
   int y;
};

int main()
{
   float pi = 3.141592;
   bool mybreak = false;
   struct Point C = {25, 25};
   float alpha = 0;
   float R1 = 0;
   int R2 = 0;
   char rotate[1];
   puts("After parameters input print symbol \"]\" to stop the program");
   puts("Enter point C ((25, 25) is coordinates of the center)");
   scanf("%i %i", &C.x, &C.y);
   puts("Enter angle (rad): ");
   scanf("%f", &alpha);
   printf("\n");
   puts("Enter distance between C and center of the circle: ");
   scanf("%f", &R1);
   printf("\n");
   puts("Enter radius of the circle: ");
   scanf("%i", &R2);
   Console_clear();
   Canvas_invertYOrientation();
   Canvas_setSize(50, 50);
   while (1 > 0)
   {
      if (R2 <= 0)
      {
         R2 = 1;
      }
      struct Point A = {C.x + R1 * sin(-alpha + pi/2), C.y + R1 * cos(-alpha + pi/2)};
      Canvas_beginDraw();
      Canvas_setColorRGB(255, 0, 0);
      Canvas_putPixel(C.x, C.y);
      Canvas_setColorRGB(0, 255, 0);
      Canvas_strokeCircle(A.x, A.y, R2);
      Canvas_endDraw();
      while (1 > 0)
      {
         rotate[0] = Console_getChar();
         if (strcmp(rotate, "w") == 0)
         {
            alpha += pi / 12;
            break;
         }
         else if (strcmp(rotate, "s") == 0)
         {
            alpha -= pi / 12;
            break;
         }
         else if (strcmp(rotate, "d") == 0)
         {
            R1 += 1;
            break;
         }
         else if (strcmp(rotate, "a") == 0)
         {
            R1 -= 1;
            break;
         }
         else if (strcmp(rotate, "e") == 0)
         {
            R2 += 1;
            break;
         }
         else if (strcmp(rotate, "q") == 0)
         {
            R2 -= 1;
            break;
         }
         else if (strcmp(rotate, "]") == 0)
         {
            mybreak = true;
            break;
         }
         else if (strcmp(rotate, "i") == 0)
         {
            C.y += 1;
            break;
         }
         else if (strcmp(rotate, "k") == 0)
         {
            C.y -= 1;
            break;
         }
         else if (strcmp(rotate, "j") == 0)
         {
            C.x -= 1;
            break;
         }
         else if (strcmp(rotate, "l") == 0)
         {
            C.x += 1;
            break;
         }
      }
      if (mybreak == true)
         break;
   }
}
