#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct StrDynArray
{
    char **array;
    size_t capacity;
    size_t length;
};

struct StringForArray
{
    char * array;
    size_t capacity;
    size_t length;
};


struct DynArray
{
    int * array;
    size_t capacity;
    size_t length;
};


struct DynArray dynarray_init();


void dynarray_deinit(struct DynArray * darr);

void dynarray_realloc(struct DynArray *pdarr);

void strdynarray_init(struct StrDynArray *p);

void strdynarray_deinit(struct StrDynArray *p);


void strdynarray_realloc(struct StrDynArray *pdarr);

//StringForArray
void string_init(struct StringForArray *p);

void string_deinit(struct StringForArray *p);


void string_realloc(struct StringForArray *pdarr);


int main(int argc, char * argv[])
{
    const char *inFileName = argv[1];
    const char *outFileName = argv[2];
    FILE *fin = fopen(inFileName, "r");
    if (fin == NULL)
    {
        fprintf(stderr, "Error opening file '%s'\n", inFileName);
        return EXIT_FAILURE;
    }
    FILE *fout = fopen(outFileName, "w");
    if (fout == NULL)
    {
        fprintf(stderr, "Error opening file '%s'\n", outFileName);
        return EXIT_FAILURE;
    }
    char taskType[10];
    char *res = fgets(taskType, 10, fin);
    if (res == NULL)
    {
        fprintf(stderr, "File is empty '%s'\n", inFileName);
        fclose(fin);
        return EXIT_FAILURE;
    }
    if (strncmp(res, "numbers", 7) == 0)
    {
        printf("Numbers!!!\n");
        struct DynArray nums = dynarray_init();
        int i = 0;
        int sum = 0;
        int sum_positive = 0;
        int sum_negative = 0;
        while (fscanf(fin, "%i", &nums.array[i]) > 0)
        {
            nums.length += 1;
            sum += nums.array[i];
            if (nums.array[i] > 0)
            {
                sum_positive += nums.array[i];
            }
            else
            {
                sum_negative += nums.array[i];
            }
            if (nums.length == nums.capacity)
            {
                dynarray_realloc(&nums);
            }
            printf("%i,", nums.array[i]);
            i += 1;
        }
        fprintf(fout, "%i ", sum_positive);
        fprintf(fout, "%i ", sum_negative);
        fprintf(fout, "%i", sum);
        dynarray_deinit(&nums);
        fclose(fin);
        fclose(fout);
        puts("");
    }
    else if (strncmp(res, "text", 4) == 0)
    {
        printf("Text!!!\n");
        struct StrDynArray strings;
        strdynarray_init(&strings);
        int i = 0;
        const int ScanningGap = 5;
        char part_of_string[ScanningGap];
        struct StringForArray full_string;
        string_init(&full_string);
        int counter = 0;
        while (feof(fin) == 0) 
        {
            fgets(part_of_string, ScanningGap, fin);
            if (full_string.capacity < full_string.length + ScanningGap + 1)
            {
                string_realloc(&full_string);
            }
            strcat(full_string.array, part_of_string);
            full_string.length += strlen(part_of_string);
            part_of_string[0] = '\0';
            if (full_string.array[full_string.length - 1] == '\n' || (feof (fin) != 0))
            {
                if (strings.capacity == strings.length)
                {
                    strdynarray_realloc(&strings);
                }
                strings.array[strings.length] = malloc(strlen(full_string.array + 1) * sizeof(char));
                strcpy(strings.array[strings.length], full_string.array);
                full_string.array[0] = '\0';
                full_string.length = 0;
                strings.length += 1; 
            }
        }
        for (int i = 0; i < strings.length ; i++)
        {
            if (strings.array[i][strlen(strings.array[i]) - 2] == '.')
            {
                fprintf(fout, "%s", strings.array[i]); 
            }
        }
        for(int i = 0; i < strings.length; i++)
        {
            printf("%s", strings.array[i]);
        }
        puts("");
        for(int i = 0; i < strings.length; i++)
        {
            free(strings.array[i]);
        }
        free(full_string.array);
        strdynarray_deinit(&strings);
        fclose(fin);
        fclose(fout);
    }
    else 
    {
        printf("Unknow task type: '%s'\n", res);
    }
}


struct DynArray dynarray_init() 
{
    const size_t initial_capacity = 16;
    return (struct DynArray) {
        malloc(initial_capacity * sizeof(int)),
        initial_capacity,
        0
    };
}

void dynarray_deinit(struct DynArray * darr)
{
    free(darr->array);
    darr->array = NULL;
    darr->capacity = 0;
    darr->length = 0;
}


void dynarray_realloc(struct DynArray *pdarr) {
    pdarr->capacity *= 2;
    int *new_array = realloc(pdarr->array, pdarr->capacity * sizeof(int));
    if (new_array == NULL) {
        free(pdarr->array);
        fprintf(stderr, "Memory reallocation error for %s", __func__);
        exit(1);
    }
    else pdarr->array = new_array;
}


void strdynarray_init(struct StrDynArray *p)
{
    p->capacity = 4;
    p->array = malloc(p->capacity * sizeof(char *));
    p->length = 0;
}


void strdynarray_deinit(struct StrDynArray *p)
{
    free(p->array);
    p->capacity = 0;
    p->length = 0;
}

void strdynarray_realloc(struct StrDynArray *pdarr) {
    pdarr->capacity *= 2;
    char **new_array = realloc(pdarr->array, pdarr->capacity * sizeof(char *));
    if (new_array == NULL) {
        free(pdarr->array);
        fprintf(stderr, "Memory reallocation error for %s", __func__);
        exit(1);
    }
    else pdarr->array = new_array;
}


void string_init(struct StringForArray *p)
{
    p->capacity = 4;
    p->array = malloc(p->capacity * sizeof(char));
    p->length = 0;
}


void string_deinit(struct StringForArray *p)
{
    free(p->array);
    p->capacity = 0;
    p->length = 0;
}


void string_realloc(struct StringForArray *pdarr) {
    pdarr->capacity *= 2;
    char *new_array = realloc(pdarr->array, pdarr->capacity * sizeof(char));
    if (new_array == NULL) 
    {
        free(pdarr->array);
        fprintf(stderr, "Memory reallocation error for %s", __func__);
        exit(1);
    }
    else pdarr->array = new_array;
}
